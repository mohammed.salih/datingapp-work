using System.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Entities;
using System.Text.Json;

namespace API.Helpers
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate next;
        private readonly ILogger<ExceptionMiddleware> logger;
        private readonly IHostEnvironment env;
        public ExceptionMiddleware(RequestDelegate next, ILogger<ExceptionMiddleware> logger, IHostEnvironment env)
        {
            this.env = env;
            this.logger = logger;
            this.next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await next(context);
            }
            catch (Exception ex)
            {
                logger.LogError(ex, ex.Message);
                context.Response.ContentType = "application/json";
                context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

                ApiException response = null;

                if (env.IsDevelopment())
                {
                    response = new ApiException(context.Response.StatusCode, ex.Message, ex.StackTrace?.ToString());
                }
                else
                {
                    response = new ApiException(context.Response.StatusCode, "Internal Server Error");
                }

                await context.Response.WriteAsJsonAsync(response);
            }
        }
    }
}