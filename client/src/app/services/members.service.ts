import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Member } from '../models/member';
import { Photo } from '../models/photo';

@Injectable({
  providedIn: 'root',
})
export class MembersService {
  baseUrl = environment.apiUrl;
  members: Member[] = [];

  constructor(private http: HttpClient) {}

  getMembers() {
    if (this.members.length > 0) return of(this.members);

    return this.http.get<Member[]>(this.baseUrl + 'Users/GetUsers').pipe(
      map((members) => {
        this.members = members;
        return members;
      })
    );
  }

  getMember(username: string) {
    const member = this.members.find((x) => x.userName === username);
    if (member) return of(member);

    return this.http.get<Member>(
      this.baseUrl + 'Users/GetUser?username=' + username
    );
  }

  updateMember(member: Member) {
    return this.http.put(this.baseUrl + 'Users/UpdateUser', member).pipe(
      map(() => {
        const index = this.members.indexOf(member);
        this.members[index] = member;
      })
    );
  }

  uploadPhoto(file: File) {
    const formData = new FormData();
    formData.append('file', file, file.name);

    return this.http.post<Photo>(this.baseUrl + 'Users/AddPhoto', formData);
  }

  setMainPhoto(photoId: number) {
    return this.http.put(
      this.baseUrl + 'Users/SetMainPhoto?photoId=' + photoId,
      {}
    );
  }

  deletePhoto(photoId: number) {
    return this.http.delete(
      this.baseUrl + 'Users/DeletePhoto?photoId=' + photoId
    );
  }
}
