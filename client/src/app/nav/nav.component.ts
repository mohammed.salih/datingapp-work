import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AccountService } from '../services/account.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css'],
})
export class NavComponent implements OnInit {
  model: any = {};
  loggedIn: boolean;
  user: any = {};

  constructor(
    private accountService: AccountService,
    private router: Router,
    private toastr: ToastrService
  ) {}

  ngOnInit(): void {
    this.getCurrentUser();
  }

  login() {
    this.accountService.login(this.model).subscribe({
      next: (user) => {
        this.loggedIn = true;
        this.router.navigate(['/members']);
      },
      error: (err) => {
        console.log(err);
        this.toastr.error(err.error);
      },
    });
  }

  logout() {
    this.loggedIn = false;
    this.accountService.logout();
    this.router.navigate(['/']);
  }

  getCurrentUser() {
    this.accountService.currentUser$.subscribe({
      next: (user) => {
        this.user = user;
        this.loggedIn = !!user;
      },
      error: (err) => {
        console.log(err);
      },
    });
  }
}
