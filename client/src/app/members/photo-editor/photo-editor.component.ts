import { Component, Input, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Member } from 'src/app/models/member';
import { Photo } from 'src/app/models/photo';
import { User } from 'src/app/models/user';
import { AccountService } from 'src/app/services/account.service';
import { MembersService } from 'src/app/services/members.service';

@Component({
  selector: 'app-photo-editor',
  templateUrl: './photo-editor.component.html',
  styleUrls: ['./photo-editor.component.css'],
})
export class PhotoEditorComponent implements OnInit {
  @Input() member: Member;
  file: File;
  user: User;

  constructor(
    private memberService: MembersService,
    private toastr: ToastrService,
    private accountService: AccountService
  ) {
    this.accountService.currentUser$.subscribe((user) => (this.user = user));
  }

  ngOnInit(): void {}

  onChange(event: Event) {
    this.file = (event.target as HTMLInputElement).files?.item(0) ?? null;
  }

  uploadPhoto() {
    if (!this.file) return;

    this.memberService.uploadPhoto(this.file).subscribe((response) => {
      this.member.photos.push(response);
      this.toastr.success('Photo uploaded');
    });
  }

  setMainPhoto(photo: Photo) {
    this.memberService.setMainPhoto(photo.id).subscribe(() => {
      this.user.photoUrl = photo.url;
      this.accountService.setCurrentUser(this.user);
      this.member.photoUrl = photo.url;

      this.member.photos.forEach((p) => {
        if (p.isMain) p.isMain = false;

        if (p.id === photo.id) p.isMain = true;
      });
    });
  }

  deletePhoto(photoId: number) {
    this.memberService.deletePhoto(photoId).subscribe(() => {
      this.member.photos = this.member.photos.filter((x) => x.id != photoId);
      this.toastr.success('Photo deleted');
    });
  }
}
